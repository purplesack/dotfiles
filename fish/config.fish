#----------------------------------------------------
# fish config file
# ~/.config/fish/config.fish
# Last updated 02 Jan 15
#----------------------------------------------------

# Autoload functions
. $HOME/.config/fish/functions/prompt.fish
. $HOME/.config/fish/functions/aliases.fish

# Path
set PATH $PATH $HOME/bin

