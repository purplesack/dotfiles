#----------------------------------------------------
# fish aliases
# ~/.config/fish/functions/aliases.fish
# Last updated 02 Jan 15
#----------------------------------------------------

# Pacman
alias pacupg 'sudo pacman -Syyu'				# Sync and upgrade packages
alias pacins 'sudo pacman -S'					# Install given package
alias pacuni 'sudo pacman -Rns'					# Uninstall, remove unneeded dependencies, config files
alias pacaur 'sudo pacman -U'					# Install .pkg.tar.xz files
alias pacorp 'sudo pacman -Rns (pacman -Qqdt)'	# Find and uninstall orphans
alias pacinf 'pacman -Qi'						# Obtain info on package
alias pacmir 'sudo pacman-mirrors -g'   		# Update mirror list

# Package Cache Manager
alias paccu 'paccacheman -u'					# Clear cache of uninstalled packages
alias pacci 'paccacheman -i'					# Clear cache of installed packages

# AUR Manager
alias aurupg 'aurman -u'						# Upgrade AUR packages
alias aurins 'aurman -i'						# Install AUR packages

# Git Manager
alias ga 'git add'
alias gr 'git rm'
alias gpm 'git push origin master'
alias gc 'git commit -m'

# Misc
alias ls 'ls --group-directories-first --time-style +"%d.%m.%Y %H:%M" --color=auto -F'
alias ll 'ls -l --group-directories-first --time-style +"%d.%m.%Y %H:%M" --color=auto -F'
alias la 'ls -la --group-directories-first --time-style +"%d.%m.%Y %H:%M" --color=auto -F'
alias grep 'grep --color tty -d skip'
alias cp "cp -i"                       		    # confirm before overwriting something
alias rm ' timeout 10 rm -iv --one-file-system' # Suspend rm after 5 seconds, ask for confirmation for even one file
alias xrdb 'xrdb ~/.Xresources'					# Reload Xresources file
alias cal 'cal -y'								# List whole year's calendar
alias makedwm 'make clean ; and make -j4'          # Recompile dwm

#VBA
alias vba 'gvbam ~/Documents/VisualBoyAdvance-1.7.2/"Legend of Zelda - A Link To The Past.gba"'

#Disable/enable firewall
alias stopfirewall 'sudo systemctl stop iptables' 	# Stop firewall
alias startfirewall 'sudo systemctl start iptables'	# Start firewall

#Disable screensaver
alias movieon 'xset -dpms; xset s off ; and xset q ; and xautolock -disable'
alias movieoff 'xset +dpms; xset s on ; and xset q ; and xautolock -enable'

# Connect to Imperial Network Drive
alias umountimperial 'sudo umount /media/imperial'
