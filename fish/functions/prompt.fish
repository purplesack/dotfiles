#----------------------------------------------------
# fish prompt
# ~/.config/fish/functions/prompt.fish
# Last updated 02 Jan 15
#----------------------------------------------------

function fish_prompt
    set_color blue
    echo \n'██ [' (pwd) '] [' (date "+%H:%M") ']'
    set_color normal
    echo '██ '
end

set fish_greeting ""


